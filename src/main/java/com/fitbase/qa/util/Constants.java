package com.fitbase.qa.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Constants {
public static WebDriver driver;
	
	public static WebDriverWait wait = new WebDriverWait(driver,70);
	public static int Page_Load_TimeOut =30;
	public static int Implicit_Wait =30;
}


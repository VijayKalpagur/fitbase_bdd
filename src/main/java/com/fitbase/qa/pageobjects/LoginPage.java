package com.fitbase.qa.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

	private WebDriver driver;
	
//By Locators	
	private By signUpLogInButton = By.xpath("//li[@id='signuplogin']/a");
	private By emailId = By.id("username");
	private By password = By.id("password");
	private By loginwithemail = By.xpath("//*[contains(text(), 'Log in with email')]");
	private By validation = By.xpath("//*[@id=\'error-msg\']");
	
	
//Constructor of the Page Class
	public LoginPage(WebDriver driver){
		this.driver=driver;	}
	
//Page action : features(behavior) of the page in the form of methods:
	
	public void clickonsignUpLogIn(){
		driver.findElement(signUpLogInButton).click();}
	
	public String getLoginPageTitle(){
		return driver.getTitle();}
	
//	public boolean isForgotPasswordLinkExists(){	
//	return driver.findElement("").isDisplayed();}
	
	public void enterUserName(String username){
		driver.findElement(emailId).sendKeys(username);}
		
	public void enterPassword(String pswd){
		driver.findElement(password).sendKeys(pswd);}
		
	public void clickonLogin(){
		driver.findElement(loginwithemail).click();}

	public boolean invalidEmailValidation(){
		return driver.findElement(validation).isDisplayed();}}


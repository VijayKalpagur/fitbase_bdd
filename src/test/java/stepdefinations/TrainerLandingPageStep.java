package stepdefinations;

import com.fitbase.qa.factory.DriverFactory;
import io.cucumber.java.en.Given;

public class TrainerLandingPageStep {

@Given("open browser and navigate to trainer home page")
public void open_browser_and_navigate_to_trainer_home_page() {
	DriverFactory.getDriver().get("https://qa.fitbase.com/trainer.html");
	
	}
}
package stepdefinations;

import java.util.List;

import org.junit.Assert;

import com.fitbase.qa.factory.DriverFactory;
import com.fitbase.qa.pageobjects.LoginPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginPageSteps {

	private LoginPage login = new LoginPage(DriverFactory.getDriver());

//***************************Scenario: User Login with Valid Credentials********************************
	
	@Given("user click on signuplogin button")
	public void user_click_on_signuplogin_button() throws Exception {
		DriverFactory.getDriver().get("https://qa.fitbase.com/");
		login.clickonsignUpLogIn();
		Thread.sleep(4000);}

	@When("user is on login landing page")
	public void user_is_on_login_landing_page() {
		String title = login.getLoginPageTitle();
		Assert.assertEquals(title,"Login");}

	@And("user enters valid credentials")
	public void user_enters_valid_credentials(DataTable dataTable) throws Exception {
		List<List<String>> Credentials = dataTable.asLists(String.class);
		for(List<String> e : Credentials) {
			login.enterUserName(e.get(0));
			Thread.sleep(2000);
			login.enterPassword(e.get(1));}
	}
	@And("user click on login button")
	public void user_click_on_login_button() throws Exception {
		login.clickonLogin();
		Thread.sleep(5000);}
	
	@Then("gets the title of the home page and verify page title")
	public void gets_the_title_of_the_home_page_and_verify_page_title() {		 String title = login.getLoginPageTitle();		 Assert.assertEquals(title, "Dashboard");}
	
	
//***************************Scenario: User Login with Invalid Credentials********************************
		
	@When("user enters Username {string} and Password {string}")
	public void user_enters_username_and_password(String username, String password) throws Exception {
		login.enterPassword(username);
		Thread.sleep(2000);
		login.enterPassword(password);}
	
	@Then("user should see a validation")
	public void user_should_see_a_validation() {
	   login.invalidEmailValidation();}
}
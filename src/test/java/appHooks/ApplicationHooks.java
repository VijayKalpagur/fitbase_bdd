package appHooks;

import java.util.Properties;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.fitbase.qa.factory.DriverFactory;
import com.fitbase.qa.util.ConfigReader;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class ApplicationHooks {
	
	private WebDriver driver;
	private DriverFactory driverFactory;
	private ConfigReader configReader;
	Properties prop;
	
@Before(order = 0)
public void getProperty(){
	configReader = new ConfigReader();
	prop = configReader.init_prop();	
}
@Before(order = 1)
public void lanuchBrowser(){
	String browserName = prop.getProperty("browser");
	driverFactory = new DriverFactory();
	driver = driverFactory.init_driver(browserName);
}	

@After(order = 1)
public void tearDown(Scenario scenario){
	if(scenario.isFailed()){
		//take Screenshot
		String screenshotName = scenario.getName().replaceAll(" ", "_");
		byte[] sourcePath = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
		scenario.attach(sourcePath,"image/png", screenshotName );
	}
}
@After(order = 0)
public void quitBrowser(){
	driver.quit();
}	
}



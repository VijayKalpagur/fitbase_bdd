package testrunners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features={"src/test/resources/Features"} , 	//path of feature file
		glue={"stepdefinations" , "appHooks"}, 	//path of stepDefination and hooks
		plugin = {"pretty","com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" ,
				"rerun:target/failedrerun.txt"}, //to generate different types of reporting
		monochrome=true, 	//display console output in proper readable format
	//	strict =true,	//it will check if any step is not defined in step definition file
		dryRun = false, //to check the mapping is proper between feature file and step defination file
		tags = "not @Skip"
		)

public class TestRunner {


}

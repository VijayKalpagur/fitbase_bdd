Feature: Login Page feature

  Background: 
    Given user click on signuplogin button
    When user is on login landing page

  @SmokeTest @RegressionTest
  Scenario: User Login with valid Credentials
    And user enters valid credentials
      | sunilkumar50@mailinator.com | password |
    And user click on login button
    Then gets the title of the home page and verify page title

  @SmokeTest @Skip
  Scenario Outline: User Login with Invalid Credentials
    And user enters Username "<username>" and Password "<password>"
    And user click on login button
    Then user should see a validation

    Examples: 
      | username                   | password |
      | sunilkuar50@mailinator.com | passwrd  |
